package numberUtil.vector

open class ShortVector(values: List<Short>):
    NumberVector<Short>(
        values = values,
        zeroValue = 0,
        add = {f, l -> (f + l).toShort() },
        subtract = {f, l -> (f - l).toShort() },
        multiply = {f, l -> (f * l).toShort() },
        fromInt = Int::toShort
    ) {
    constructor(size: Int, initializer: (Int) -> Short): this(List(size, initializer))
}

open class MutableShortVector(values: MutableList<Short>):
    MutableNumberVector<Short>(
        values = values,
        zeroValue = 0,
        add = {f, l -> (f + l).toShort() },
        subtract = {f, l -> (f - l).toShort() },
        multiply = {f, l -> (f * l).toShort() },
        fromInt = Int::toShort
    ) {
    constructor(size: Int, initializer: (Int) -> Short): this(MutableList(size, initializer))
}