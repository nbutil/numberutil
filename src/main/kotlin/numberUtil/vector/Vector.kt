package numberUtil.vector

/**
 * Defines a [Vector]
 */
interface Vector<T>: List<T> {
    /** @property [values] The values used in the [Vector] */
    val values: List<T>

    /**
     * Multiplies all values in this [Vector] by [other]
     *
     * @param [other] The element to multiply all values in this [Vector] by
     *
     * @return [Vector]
     */
    operator fun times(other: T): Vector<T>

    /**
     * Returns the dot product of this [Vector] and [other]
     *
     * @param [other] The other [Vector] to get the dot product from
     *
     * @return [T]
     */
    infix fun dot(other: Vector<T>): T

    /** @see [dot] */
    operator fun times(other: Vector<T>): T

    /**
     * Returns a [Vector] containing the result of adding each value in [other] to each corresponding value in this [Vector]
     *
     * @param [other] The other [Vector] to add values from
     *
     * @return [Vector]
     */
    operator fun plus(other: Vector<T>): Vector<T>

    /**
     * Returns a [Vector] containing the result of subtracting each value in [other] from each corresponding value in this [Vector]
     *
     * @param [other] The other [Vector] to get the values subtracted
     *
     * @return [Vector]
     */
    operator fun minus(other: Vector<T>): Vector<T>

    /**
     * Returns this [Vector] casted to [Vector]
     *
     * @return [Vector]
     */
    fun asVector(): Vector<T> = this

    /**
     * Compares the size of this [Vector] to the size of [other]. If they are not equal, will throw [InvalidVectorDimensions]
     *
     * @param [other] The vector to compare sizes with
     * @param [operationName] The name of the operation being performed
     *
     * @throws [InvalidVectorDimensions]
     */
    fun compareOtherSize(other: Vector<T>, operationName: String) {
        if (this.size != other.size) {
            throw InvalidVectorDimensions("Other vector size (${other.size}) must be equal to this vector size ($size) for vector $operationName")
        }
    }
}
