package numberUtil.vector

open class MutableNumberVector<T: Number>(
    override val values: MutableList<T>,
    zeroValue: T,
    add: (T, T) -> T,
    subtract: (T, T) -> T,
    multiply: (T, T) -> T,
    fromInt: (Int) -> T
): NumberVector<T>(values, zeroValue, add, subtract, multiply, fromInt), MutableVector<T>, MutableList<T> by values {

    override operator fun times(other: T): MutableNumberVector<T> =
        mutableCopyOf(values = MutableList(size) { multiply(this.values[it], other) } )

    override fun plus(other: Vector<T>): MutableNumberVector<T> {
        compareOtherSize(other, "addition")
        return mutableCopyOf(values = MutableList(size) { add(this.values[it], other[it])})
    }

    override fun minus(other: Vector<T>): MutableNumberVector<T> {
        compareOtherSize(other, "subtraction")
        return mutableCopyOf(values = MutableList(size) { subtract(this.values[it], other[it])})
    }

    override operator fun timesAssign(other: T) {
        for((index, value) in this.withIndex()) {
            this[index] = multiply(value, other)
        }
    }

    override operator fun plusAssign(other: Vector<T>) {
        compareOtherSize(other, "addition (in place)")
        for ((index, value) in this.withIndex()) {
            this[index] = add(value, other[index])
        }
    }

    override operator fun minusAssign(other: Vector<T>){
        compareOtherSize(other, "subtraction (in place)")
        for ((index, value) in this.withIndex()) {
            this[index] = subtract(value, other[index])
        }
    }
}