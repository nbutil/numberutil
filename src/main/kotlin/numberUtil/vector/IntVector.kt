package numberUtil.vector

open class IntVector(values: List<Int>):
    NumberVector<Int>(
        values = values,
        zeroValue = 0,
        add = {f, l -> f + l },
        subtract = {f, l -> f - l },
        multiply = {f, l -> f * l },
        fromInt = Int::toInt
    ) {
    constructor(size: Int, initializer: (Int) -> Int): this(List(size, initializer))
}

open class MutableIntVector(values: MutableList<Int>):
    MutableNumberVector<Int>(
        values = values,
        zeroValue = 0,
        add = {f, l -> f + l },
        subtract = {f, l -> f - l },
        multiply = {f, l -> f * l },
        fromInt = Int::toInt
    ) {
    constructor(size: Int, initializer: (Int) -> Int): this(MutableList(size, initializer))
}