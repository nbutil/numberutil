package numberUtil.vector

class InvalidVectorDimensions(override val message: String): Exception(message)
