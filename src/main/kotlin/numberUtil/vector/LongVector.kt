package numberUtil.vector

open class LongVector(values: List<Long>):
    NumberVector<Long>(
        values = values,
        zeroValue = 0,
        add = {f, l -> f + l },
        subtract = {f, l -> f - l },
        multiply = {f, l -> f * l },
        fromInt = Int::toLong
    ) {
    constructor(size: Int, initializer: (Int) -> Long): this(List(size, initializer))
}

open class MutableLongVector(values: MutableList<Long>):
    MutableNumberVector<Long>(
        values = values,
        zeroValue = 0,
        add = {f, l -> f + l },
        subtract = {f, l -> f - l },
        multiply = {f, l -> f * l },
        fromInt = Int::toLong
    ) {
    constructor(size: Int, initializer: (Int) -> Long): this(MutableList(size, initializer))
}