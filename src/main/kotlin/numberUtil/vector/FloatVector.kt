package numberUtil.vector

open class FloatVector(values: List<Float>):
    NumberVector<Float>(
        values = values,
        zeroValue = 0.0f,
        add = {f, l -> f + l },
        subtract = {f, l -> f - l },
        multiply = {f, l -> f * l },
        fromInt = Int::toFloat
    ) {
    constructor(size: Int, initializer: (Int) -> Float): this(List(size, initializer))
}

open class MutableFloatVector(values: MutableList<Float>):
    MutableNumberVector<Float>(
        values = values,
        zeroValue = 0.0f,
        add = {f, l -> f + l },
        subtract = {f, l -> f - l },
        multiply = {f, l -> f * l },
        fromInt = Int::toFloat
    ) {
    constructor(size: Int, initializer: (Int) -> Float): this(MutableList(size, initializer))
}