package numberUtil.vector

open class ByteVector(values: List<Byte>):
    NumberVector<Byte>(
        values = values,
        zeroValue = 0,
        add = {f, l -> (f + l).toByte() },
        subtract = {f, l -> (f - l).toByte() },
        multiply = {f, l -> (f * l).toByte() },
        fromInt = Int::toByte
    ) {
    constructor(size: Int, initializer: (Int) -> Byte): this(List(size, initializer))
}

open class MutableByteVector(values: MutableList<Byte>):
    MutableNumberVector<Byte>(
        values = values,
        zeroValue = 0,
        add = {f, l -> (f + l).toByte() },
        subtract = {f, l -> (f - l).toByte() },
        multiply = {f, l -> (f * l).toByte() },
        fromInt = Int::toByte
    ) {
    constructor(size: Int, initializer: (Int) -> Byte): this(MutableList(size, initializer))
}