package numberUtil.vector

/**
 * Defines a [MutableVector], a [Vector] that you can add to, take away from and change the contents of.
 */
interface MutableVector<T>: Vector<T>, MutableList<T> {
    /** @property [values] The values that are mutable */
    override val values: MutableList<T>

    /**
     * Multiplies each value in this [MutableVector] by [other]
     *
     * @param [other] The value to multiply each value by
     */
    operator fun timesAssign(other: T)

    /**
     * Adds each value in [other] to the corresponding value in this [MutableVector]
     *
     * @param [other] The [Vector] to get the added values from
     */
    operator fun plusAssign(other: Vector<T>)

    /**
     * Subtracts each value in [other] from the corresponding value in this [MutableVector]
     *
     * @param [other] The [Vector] to get the subtracted values from.
     */
    operator fun minusAssign(other: Vector<T>)
}
