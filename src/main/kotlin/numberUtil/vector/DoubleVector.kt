package numberUtil.vector

open class DoubleVector(values: List<Double>):
    NumberVector<Double>(
        values = values,
        zeroValue = 0.0,
        add = {f, l -> f + l },
        subtract = {f, l -> f - l },
        multiply = {f, l -> f * l },
        fromInt = Int::toDouble
    ) {
    constructor(size: Int, initializer: (Int) -> Double): this(List(size, initializer))
}

open class MutableDoubleVector(values: MutableList<Double>):
    MutableNumberVector<Double>(
        values = values,
        zeroValue = 0.0,
        add = {f, l -> f + l },
        subtract = {f, l -> f - l },
        multiply = {f, l -> f * l },
        fromInt = Int::toDouble
    ) {
    constructor(size: Int, initializer: (Int) -> Double): this(MutableList(size, initializer))
}