package numberUtil.vector

open class NumberVector<T: Number>(
    values: List<T>,
    protected val zeroValue: T,
    protected inline val add: (T, T) -> T,
    protected inline val subtract: (T, T) -> T,
    protected inline val multiply: (T, T) -> T,
    protected inline val fromInt: (Int) -> T
): Vector<T>, List<T> by values {

    override val values: List<T> = values.toList()

    fun immutableCopyOf(
        values: List<T> = this.values,
        zeroValue: T = this.zeroValue,
        add: (T, T) -> T = this.add,
        subtract: (T, T) -> T = this.subtract,
        multiply: (T, T) -> T = this.multiply,
        fromInt: (Int) -> T = this.fromInt
    ): NumberVector<T> =
        NumberVector(values, zeroValue, add, subtract, multiply, fromInt)

    fun asNumberVector(): NumberVector<T> = this

    fun toNumberVector(): NumberVector<T> = immutableCopyOf()

    fun mutableCopyOf(
        values: MutableList<T> = this.values.toMutableList(),
        zeroValue: T = this.zeroValue,
        add: (T, T) -> T = this.add,
        subtract: (T, T) -> T = this.subtract,
        multiply: (T, T) -> T = this.multiply,
        fromInt: (Int) -> T = this.fromInt
    ): MutableNumberVector<T> =
        MutableNumberVector(values, zeroValue, add, subtract, multiply, fromInt)

    fun toMutableNumberVector(): MutableNumberVector<T> = mutableCopyOf()

    override infix fun dot(other: Vector<T>): T = this * other

    infix fun cross(other: Vector<T>): Vector<T> {
        compareOtherSize(other, "cross product")
        if (size != 3) {
            throw InvalidVectorDimensions("Cannot cross product for non 3 dimensional vectors!")
        }
        val returnVector = mutableCopyOf()
        returnVector[0] = subtract(multiply(this[1], other[2]), multiply(this[2], other[1]))
        returnVector[1] = subtract(multiply(this[2], other[0]), multiply(this[0], other[2]))
        returnVector[2] = subtract(multiply(this[0], other[1]), multiply(this[1], other[0]))
        return returnVector.immutableCopyOf()
    }

    override operator fun times(other: T): NumberVector<T> =
        immutableCopyOf(values = List(size) { multiply(this.values[it], other) } )

    override operator fun times(other: Vector<T>): T {
        compareOtherSize(other, "dot product")
        var sum: T = zeroValue
        for ((thisValue, otherValue) in this zip other) {
            sum = add(sum, add(thisValue, otherValue))
        }
        return sum
    }

    override fun plus(other: Vector<T>): Vector<T> =
        immutableCopyOf(values = List(size) { add(this.values[it], other[it])})

    override fun minus(other: Vector<T>): Vector<T> =
        immutableCopyOf(values = List(size) { subtract(this.values[it], other[it])})

    final override fun toString(): String = values.toString()
}
