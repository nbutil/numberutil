package numberUtil.byteArray

/** @author: Curtis Woodard <nbness1337@gmail.com>
 *
 * @sample byteArrayOf(0x00, 0x01).toShort()
 */

/**
 * Converts the [ByteArray] to a [Byte] in [bigEndian] order
 *
 * @param [bigEndian] Whether or not you want to interpret the [ByteArray] in Big Endian or Little Endian
 *
 * @return [Byte]
 */
fun ByteArray.toByte(bigEndian: Boolean = true): Byte =
    if (this.isEmpty())
        0
    else
        this[0]

/**
 * Converts the [ByteArray] to a [Short] in [bigEndian] order
 *
 * @param [bigEndian] Whether or not you want to interpret the [ByteArray] in Big Endian or Little Endian
 *
 * @return [Short]
 */
fun ByteArray.toShort(bigEndian: Boolean = true): Short = this.padded(Short.SIZE_BYTES).run {
    if (bigEndian)
        ((this[0].toInt() and 0xFF shl 8) + (this[1].toInt() and 0xFF)).toShort()
    else
        ((this[1].toInt() and 0xFF shl 8) + (this[0].toInt() and 0xFF shl 0)).toShort()
}

/**
 * Converts the [ByteArray] to a [Int] in [bigEndian] order
 *
 * @param [bigEndian] Whether or not you want to interpret the [ByteArray] in Big Endian or Little Endian
 *
 * @return [Int]
 */
fun ByteArray.toInt(bigEndian: Boolean = true): Int = this.padded(Int.SIZE_BYTES).run {
    if (bigEndian)
        (this[0].toInt() and 0xFF shl 24) +
        (this[1].toInt() and 0xFF shl 16) +
        (this[2].toInt() and 0xFF shl 8) +
        (this[3].toInt() and 0xFF)
    else
        (this[3].toInt() and 0xFF shl 24) +
        (this[2].toInt() and 0xFF shl 16) +
        (this[1].toInt() and 0xFF shl 8) +
        (this[0].toInt() and 0xFF)
}

/**
 * Converts the [ByteArray] to a [Long] in [bigEndian] order
 *
 * @param [bigEndian] Whether or not you want to interpret the [ByteArray] in Big Endian or Little Endian
 *
 * @return [Long]
 */
fun ByteArray.toLong(bigEndian: Boolean = true): Long = this.padded(Long.SIZE_BYTES).run {
    if (bigEndian)
        (this[0].toLong() and 0xFF shl 56) +
        (this[1].toLong() and 0xFF shl 48) +
        (this[2].toLong() and 0xFF shl 40) +
        (this[3].toLong() and 0xFF shl 32) +
        (this[4].toLong() and 0xFF shl 24) +
        (this[5].toLong() and 0xFF shl 16) +
        (this[6].toLong() and 0xFF shl 8) +
        (this[7].toLong() and 0xFF)
    else
        (this[7].toLong() and 0xFF shl 56) +
        (this[6].toLong() and 0xFF shl 48) +
        (this[5].toLong() and 0xFF shl 40) +
        (this[4].toLong() and 0xFF shl 32) +
        (this[3].toLong() and 0xFF shl 24) +
        (this[2].toLong() and 0xFF shl 16) +
        (this[1].toLong() and 0xFF shl 8) +
        (this[0].toLong() and 0xFF)
}


/**
 * Converts the [ByteArray] to a [Float] in [bigEndian] order
 *
 * @param [bigEndian] Whether or not you want to interpret the [ByteArray] in Big Endian or Little Endian
 *
 * @return [Float]
 */
fun ByteArray.toFloat(bigEndian: Boolean=true): Float =
    Float.fromBits(Int.fromByteArray(this, bigEndian))

/**
 * Converts the [ByteArray] to a [Double] in [bigEndian] order
 *
 * @param [bigEndian] Whether or not you want to interpret the [ByteArray] in Big Endian or Little Endian
 *
 * @return [Double]
 */
fun ByteArray.toDouble(bigEndian: Boolean=true): Double =
    Double.fromBits(Long.fromByteArray(this, bigEndian))
