package numberUtil.byteArray

/** @author: Curtis Woodard <nbness1337@gmail.com>
 *
 * Small note, these are called by their type.
 * @sample Byte.fromByteArray(byteArrayOf(0))
 */

/**
 * Pads (or cuts) the [ByteArray] to [toSize]
 *
 * @return [ByteArray]
 */
internal fun ByteArray.padded(toSize: Int): ByteArray =
    if (this.size >= toSize)
        this
    else
        ByteArray(this.size - toSize) { 0 } + this

/**
 * Converts the [ByteArray] to a [Byte] in [bigEndian] order
 *
 * @param [bytes] The [ByteArray] to interpret
 * @param [bigEndian] Whether or not you want to interpret the [ByteArray] as Big Endian or Little Endian
 *
 * @return [Byte]
 */
fun Byte.Companion.fromByteArray(bytes: ByteArray, bigEndian: Boolean = true): Byte =
    if (bytes.isEmpty())
        0
    else
        bytes[0]

/**
 * Converts the [ByteArray] to a [Short] in [bigEndian] order
 *
 * @param [bytes] The [ByteArray] to interpret
 * @param [bigEndian] Whether or not you want to interpret the [ByteArray] as Big Endian or Little Endian
 *
 * @return [Short]
 */
fun Short.Companion.fromByteArray(bytes: ByteArray, bigEndian: Boolean = true): Short = bytes.padded(Short.SIZE_BYTES).run {
    if (bigEndian)
        ((this[0].toInt() and 0xFF shl 8) + (this[1].toInt() and 0xFF)).toShort()
    else
        ((this[1].toInt() and 0xFF shl 8) + (this[0].toInt() and 0xFF shl 0)).toShort()
}

/**
 * Converts the [ByteArray] to a [Int] in [bigEndian] order
 *
 * @param [bytes] The [ByteArray] to interpret
 * @param [bigEndian] Whether or not you want to interpret the [ByteArray] as Big Endian or Little Endian
 *
 * @return [Int]
 */
fun Int.Companion.fromByteArray(bytes: ByteArray, bigEndian: Boolean = true): Int = bytes.padded(Int.SIZE_BYTES).run {
    if (bigEndian)
        (this[0].toInt() and 0xFF shl 24) +
        (this[1].toInt() and 0xFF shl 16) +
        (this[2].toInt() and 0xFF shl 8) +
        (this[3].toInt() and 0xFF)
    else
        (this[3].toInt() and 0xFF shl 24) +
        (this[2].toInt() and 0xFF shl 16) +
        (this[1].toInt() and 0xFF shl 8) +
        (this[0].toInt() and 0xFF)
}

/**
 * Converts the [ByteArray] to a [Long] in [bigEndian] order
 *
 * @param [bytes] The [ByteArray] to interpret
 * @param [bigEndian] Whether or not you want to interpret the [ByteArray] as Big Endian or Little Endian
 *
 * @return [Long]
 */
fun Long.Companion.fromByteArray(bytes: ByteArray, bigEndian: Boolean = true): Long = bytes.padded(Long.SIZE_BYTES).run {
    if (bigEndian)
        (this[0].toLong() and 0xFF shl 56) +
        (this[1].toLong() and 0xFF shl 48) +
        (this[2].toLong() and 0xFF shl 40) +
        (this[3].toLong() and 0xFF shl 32) +
        (this[4].toLong() and 0xFF shl 24) +
        (this[5].toLong() and 0xFF shl 16) +
        (this[6].toLong() and 0xFF shl 8) +
        (this[7].toLong() and 0xFF)
    else
        (this[7].toLong() and 0xFF shl 56) +
        (this[6].toLong() and 0xFF shl 48) +
        (this[5].toLong() and 0xFF shl 40) +
        (this[4].toLong() and 0xFF shl 32) +
        (this[3].toLong() and 0xFF shl 24) +
        (this[2].toLong() and 0xFF shl 16) +
        (this[1].toLong() and 0xFF shl 8) +
        (this[0].toLong() and 0xFF)
}


/**
 * Converts the [ByteArray] to a [Float] in [bigEndian] order
 *
 * @param [bytes] The [ByteArray] to interpret
 * @param [bigEndian] Whether or not you want to interpret the [ByteArray] as Big Endian or Little Endian
 *
 * @return [Float]
 */
fun Float.Companion.fromByteArray(bytes: ByteArray, bigEndian: Boolean=true): Float =
    Float.fromBits(Int.fromByteArray(bytes, bigEndian))

/**
 * Converts the [ByteArray] to a [Double] in [bigEndian] order
 *
 * @param [bytes] The [ByteArray] to interpret
 * @param [bigEndian] Whether or not you want to interpret the [ByteArray] as Big Endian or Little Endian
 *
 * @return [Double]
 */
fun Double.Companion.fromByteArray(bytes: ByteArray, bigEndian: Boolean=true): Double =
    Double.fromBits(Long.fromByteArray(bytes, bigEndian))
