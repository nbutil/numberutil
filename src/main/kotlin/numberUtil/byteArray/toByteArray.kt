package numberUtil.byteArray

/** @author: Curtis Woodard <nbness1337@gmail.com> */

/**
 * Converts the [Byte] in to a [ByteArray]
 *
 * Whether or not you want to put the [ByteArray] in Big Endian or Little Endian
 *
 * @return [ByteArray]
 */
fun Byte.toByteArray(bigEndian: Boolean=true): ByteArray = ByteArray(1) { this }

/**
 * Converts the [Short] in to a [ByteArray]
 *
 * Whether or not you want to put the [ByteArray] in Big Endian or Little Endian
 *
 * @return [ByteArray]
 */
fun Short.toByteArray(bigEndian: Boolean=true): ByteArray {
    val proxyInt = this.toInt()
    return if (bigEndian)
        byteArrayOf(
            (proxyInt ushr 8 and 0xFF).toByte(),
            (proxyInt and 0xFF).toByte()
        )

    else
        byteArrayOf(
            (proxyInt and 0xFF).toByte(),
            (proxyInt ushr 8 and 0xFF).toByte()
        )
}

/**
 * Converts the [Int] in to a [ByteArray]
 *
 * Whether or not you want to put the [ByteArray] in Big Endian or Little Endian
 *
 * @return [ByteArray]
 */
fun Int.toByteArray(bigEndian: Boolean=true): ByteArray {
    return if (bigEndian)
        byteArrayOf(
            (this ushr 24 and 0xFF).toByte(),
            (this ushr 16 and 0xFF).toByte(),
            (this ushr 8 and 0xFF).toByte(),
            (this and 0xFF).toByte()
        )

    else
        byteArrayOf(
            (this and 0xFF).toByte(),
            (this ushr 8 and 0xFF).toByte(),
            (this ushr 16 and 0xFF).toByte(),
            (this ushr 24 and 0xFF).toByte()
        )
}


/**
 * Converts the [Int] in to a [ByteArray]
 *
 * Whether or not you want to put the [ByteArray] in Big Endian or Little Endian
 *
 * @return [ByteArray]
 */
fun Long.toByteArray(bigEndian: Boolean=true): ByteArray {
    return if (bigEndian)
        byteArrayOf(
            (this ushr 56 and 0xFF).toByte(),
            (this ushr 48 and 0xFF).toByte(),
            (this ushr 40 and 0xFF).toByte(),
            (this ushr 32 and 0xFF).toByte(),
            (this ushr 24 and 0xFF).toByte(),
            (this ushr 16 and 0xFF).toByte(),
            (this ushr 8 and 0xFF).toByte(),
            (this and 0xFF).toByte()
        )
    else
        byteArrayOf(
            (this and 0xFF).toByte(),
            (this ushr 8 and 0xFF).toByte(),
            (this ushr 16 and 0xFF).toByte(),
            (this ushr 24 and 0xFF).toByte(),
            (this ushr 32 and 0xFF).toByte(),
            (this ushr 40 and 0xFF).toByte(),
            (this ushr 48 and 0xFF).toByte(),
            (this ushr 56 and 0xFF).toByte()
        )
}

/**
 * Converts the bits that make up the [Float] in to a [ByteArray]
 *
 * Whether or not you want to put the [ByteArray] in Big Endian or Little Endian
 *
 * @return [ByteArray]
 */
fun Float.toByteArray(bigEndian: Boolean=true): ByteArray = this.toRawBits().toByteArray(bigEndian=bigEndian)

/**
 * Converts the bits that make up the [Double] in to a [ByteArray]
 *
 * Whether or not you want to put the [ByteArray] in Big Endian or Little Endian
 *
 * @return [ByteArray]
 */
fun Double.toByteArray(bigEndian: Boolean=true): ByteArray = this.toRawBits().toByteArray(bigEndian=bigEndian)

