package numberUtil.pair

import numberUtil.access.getShort
import numberUtil.access.setShort

class DenseShortPair(first: Short, second: Short) {
    private val internalValue = 0.setShort(0, first).setShort(1, second)

    val first: Short
        get() = internalValue.getShort(0)

    val second: Short
        get() = internalValue.getShort(1)

    operator fun component1(): Short = first

    operator fun component2(): Short = second

    override fun toString(): String = "($first, $second)"
}
