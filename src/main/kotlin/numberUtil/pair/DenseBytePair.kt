package numberUtil.pair

import numberUtil.access.getByte
import numberUtil.access.setByte

class DenseBytePair(first: Byte, second: Byte) {
    private val internalValue = (0.toShort()).setByte(0, first).setByte(1, second)

    val first: Byte
        get() = internalValue.getByte(0)

    val second: Byte
        get() = internalValue.getByte(1)

    operator fun component1(): Byte = first

    operator fun component2(): Byte = second

    override fun toString(): String = "($first, $second)"
}
