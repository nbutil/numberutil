package numberUtil.pair

import numberUtil.access.getInt
import numberUtil.access.setInt

class DenseIntPair(first: Int, second: Int) {
    private val internalValue = 0L.setInt(0, first).setInt(1, second)

    val first: Int
        get() = internalValue.getInt(0)

    val second: Int
        get() = internalValue.getInt(1)

    operator fun component1(): Int = first

    operator fun component2(): Int = second

    override fun toString(): String = "($first, $second)"
}
