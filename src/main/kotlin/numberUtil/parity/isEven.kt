package numberUtil.parity

/** @author: Curtis Woodard <nbness1337@gmail.com> */

/**
 * Checks if the [Byte] value is even
 *
 * @return [Boolean]
 */
fun Byte.isEven(): Boolean = (this.toInt() and 1) == 0

/**
 * Checks if the [Short] value is even
 *
 * @return [Boolean]
 */
fun Short.isEven(): Boolean = (this.toInt() and 1) == 0

/**
 * Checks if the [Int] value is even
 *
 * @return [Boolean]
 */
fun Int.isEven(): Boolean = (this and 1) == 0

/**
 * Checks if the [Long] value is even
 *
 * @return [Boolean]
 */
fun Long.isEven(): Boolean = (this and 1) == 0L
