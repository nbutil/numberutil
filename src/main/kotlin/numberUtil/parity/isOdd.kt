package numberUtil.parity

/** @author: Curtis Woodard <nbness1337@gmail.com> */

/**
 * Checks if the [Byte] value is Odd
 *
 * @return [Boolean]
 */
fun Byte.isOdd(): Boolean = (this.toInt() and 1) == 1

/**
 * Checks if the [Short] value is Odd
 *
 * @return [Boolean]
 */
fun Short.isOdd(): Boolean = (this.toInt() and 1) == 1

/**
 * Checks if the [Int] value is Odd
 *
 * @return [Boolean]
 */
fun Int.isOdd(): Boolean = (this and 1) == 1

/**
 * Checks if the [Long] value is Odd
 *
 * @return [Boolean]
 */
fun Long.isOdd(): Boolean = (this and 1) == 1L
