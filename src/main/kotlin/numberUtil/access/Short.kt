package numberUtil.access

/** @author: Curtis Woodard <nbness1337@gmail.com> */

/**
 * Gets the un-shifted value of [bitIndex]
 *
 * @param [bitIndex] The index of the bit to get
 *
 * @return [Int]
 */
fun Short.getBitValue(bitIndex: Int): Int =
    if (bitIndex in 0 until Short.SIZE_BITS)
        this.toInt() and (1 shl bitIndex)
    else
        throw IndexOutOfBoundsException("Invalid bit $bitIndex for Short")

/**
 * Gets the on/off value of [bitIndex]
 *
 * @param [bitIndex] The index of the bit to get
 *
 * @return [Int]
 */
fun Short.getBit(bitIndex: Int): Int =
    getBitValue(bitIndex) ushr bitIndex

/**
 * Returns the result of setting the [bitIndex]th bit to [bitValue]
 *
 * @param [bitIndex] The index of the bit to set to [bitValue]
 * @param [bitValue] The value to set the [bitIndex]th bit to
 *
 * @return [Short]
 * @throws [IndexOutOfBoundsException]
 */
fun Short.setBit(bitIndex: Int, bitValue: Boolean): Short {
    if (bitIndex !in 0 until Short.SIZE_BITS) throw IndexOutOfBoundsException("Invalid bit index $bitIndex for Short")
    val position = bitIndex
    return ((this.toInt() and (0x1 shl position).inv()) + ((if (bitValue) 0x1 else 0x0) shl position)).toShort()
}


/**
 * Gets the [byteIndex]th [Byte]
 *
 * @param [byteIndex] The byte to get
 *
 * @return [Byte]
 */
fun Short.getByte(byteIndex: Int): Byte =
    if (byteIndex in 0 until Short.SIZE_BYTES)
        (this.toInt() and (0xFF shl (Byte.SIZE_BITS * byteIndex)) ushr (Byte.SIZE_BITS * byteIndex)).toByte()
    else
        throw IndexOutOfBoundsException("Invalid byte $byteIndex for Short")

/**
 * Sets the [byteIndex]th byte in the number to [byteValue]
 *
 * @param [byteIndex] The index of the byte to set
 * @param [byteValue] The value to set the byte to
 *
 * @return [Short]
 */
fun Short.setByte(byteIndex: Int, byteValue: Byte): Short {
    if (byteIndex !in 0 until Short.SIZE_BYTES) throw IndexOutOfBoundsException("Invalid byte $byteIndex for Int")
    val position = (Byte.SIZE_BITS * byteIndex)
    return ((this.toInt() and (0xFF shl position).inv()) + (byteValue.toInt() shl position)).toShort()
}

operator fun Short.component1(): Byte = getByte(0)

operator fun Short.component2(): Byte = getByte(1)
