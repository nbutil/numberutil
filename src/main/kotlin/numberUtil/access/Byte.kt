package numberUtil.access

/** @author: Curtis Woodard <nbness1337@gmail.com> */

/**
 * Gets the un-shifted value of [bitIndex]
 *
 * @param [bitIndex] The index of the bit to get
 *
 * @return [Int]
 */
fun Byte.getBitValue(bitIndex: Int): Int =
    if (bitIndex in 0 until Byte.SIZE_BITS)
        this.toInt() and (1 shl bitIndex)
    else
        throw IndexOutOfBoundsException("Invalid bit $bitIndex for Byte")

/**
 * Gets the on/off value of [bitIndex]
 *
 * @param [bitIndex] The index of the bit to get
 *
 * @return [Int]
 */
fun Byte.getBit(bitIndex: Int): Int =
    getBitValue(bitIndex) ushr bitIndex


/**
 * Returns the result of setting the [bitIndex]th bit to [bitValue]
 *
 * @param [bitIndex] The index of the bit to set to [bitValue]
 * @param [bitValue] The value to set the [bitIndex]th bit to
 *
 * @return [Byte]
 * @throws [IndexOutOfBoundsException]
 */
fun Byte.setBit(bitIndex: Int, bitValue: Boolean): Byte {
    if (bitIndex !in 0 until Byte.SIZE_BITS) throw IndexOutOfBoundsException("Invalid bit index $bitIndex for Byte")
    return ((this.toInt() and (0x1 shl bitIndex).inv()) + ((if (bitValue) 0x1 else 0x0) shl bitIndex)).toByte()
}