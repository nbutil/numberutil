package numberUtil.access

/** @author: Curtis Woodard <nbness1337@gmail.com> */

/**
 * Gets the un-shifted value of [bitIndex]
 *
 * @param [bitIndex] The index of the bit to get
 *
 * @return [Int]
 */
fun Int.getBitValue(bitIndex: Int): Int =
    if (bitIndex in 0 until Int.SIZE_BITS)
        this and (1 shl bitIndex)
    else
        throw IndexOutOfBoundsException("Invalid bit $bitIndex for Int")

/**
 * Gets the on/off value of [bitIndex]
 *
 * @param [bitIndex] The index of the bit to get
 *
 * @return [Int]
 */
fun Int.getBit(bitIndex: Int): Int =
    getBitValue(bitIndex) ushr bitIndex

/**
 * Returns the result of setting the [bitIndex]th bit to [bitValue]
 *
 * @param [bitIndex] The index of the bit to set to [bitValue]
 * @param [bitValue] The value to set the [bitIndex]th bit to
 *
 * @return [Short]
 * @throws [IndexOutOfBoundsException]
 */
fun Int.setBit(bitIndex: Int, bitValue: Boolean): Int {
    if (bitIndex !in 0 until Short.SIZE_BITS) throw IndexOutOfBoundsException("Invalid bit index $bitIndex for Short")
    val position = bitIndex
    return ((this and (0x1 shl position).inv()) + ((if (bitValue) 0x1 else 0x0) shl position))
}

/**
 * Gets the [byteIndex]th [Byte]
 *
 * @param [byteIndex] The byte to get
 *
 * @return [Byte]
 */
fun Int.getByte(byteIndex: Int): Byte =
    if (byteIndex in 0 until Int.SIZE_BYTES)
        (this and (0xFF shl (Byte.SIZE_BITS * byteIndex)) ushr (Byte.SIZE_BITS * byteIndex)).toByte()
    else
        throw IndexOutOfBoundsException("Invalid byte $byteIndex for Int")

/**
 * Sets the [byteIndex]th byte in the number to [byteValue]
 *
 * @param [byteIndex] The index of the byte to set
 * @param [byteValue] The value to set the byte to
 *
 * @return [Int]
 */
fun Int.setByte(byteIndex: Int, byteValue: Byte): Int {
    if (byteIndex !in 0 until Int.SIZE_BYTES) throw IndexOutOfBoundsException("Invalid byte $byteIndex for Int")
    val position = (Byte.SIZE_BITS * byteIndex)
    return (this and (0xFF shl position).inv()) + (byteValue.toInt() shl position)
}

operator fun Int.component1(): Byte = getByte(0)

operator fun Int.component2(): Byte = getByte(1)

operator fun Int.component3(): Byte = getByte(2)

operator fun Int.component4(): Byte = getByte(3)



/**
 * Gets the [shortIndex]th [Short]
 *
 * @param [shortIndex] The byte to get
 *
 * @return [Short]
 */
fun Int.getShort(shortIndex: Int): Short =
    if (shortIndex in 0 until (Int.SIZE_BYTES / Short.SIZE_BYTES))
        (this and (0xFFFF shl (Short.SIZE_BITS * shortIndex)) ushr (Short.SIZE_BITS * shortIndex)).toShort()
    else
        throw IndexOutOfBoundsException("Invalid short index $shortIndex for Int")

/**
 * Returns the result of setting the [shortIndex]th serial [Short] to [shortValue]
 *
 * @param [shortIndex] The short to set
 * @param [shortValue] The value to set the [shortIndex]th [Short] to
 *
 * @return [Int]
 */
fun Int.setShort(shortIndex: Int, shortValue: Short): Int {
    if (shortIndex !in 0 until (Int.SIZE_BYTES / Short.SIZE_BYTES)) throw IndexOutOfBoundsException("Invalid byte $shortIndex for Int")
    val position = (Short.SIZE_BITS * shortIndex)
    return (this and (0xFFFF shl position).inv()) + (shortValue.toInt() shl position)
}
