package numberUtil.access

/** @author: Curtis Woodard <nbness1337@gmail.com> */

/**
 * Gets the un-shifted value of [bitIndex]
 *
 * @param [bitIndex] The index of the bit to get
 *
 * @return [Int]
 */
fun Long.getBitValue(bitIndex: Int): Long =
    if (bitIndex in 0 until Long.SIZE_BITS)
        this and (1L shl bitIndex)
    else
        throw IndexOutOfBoundsException("Invalid bit $bitIndex for Long")

/**
 * Gets the on/off value of [bitIndex]
 *
 * @param [bitIndex] The index of the bit to get
 *
 * @return [Int]
 */
fun Long.getBit(bitIndex: Int): Int =
    (getBitValue(bitIndex) ushr bitIndex).toInt()

/**
 * Returns the result of setting the [bitIndex]th bit to [bitValue]
 *
 * @param [bitIndex] The index of the bit to set to [bitValue]
 * @param [bitValue] The value to set the [bitIndex]th bit to
 *
 * @return [Long]
 * @throws [IndexOutOfBoundsException]
 */
fun Long.setBit(bitIndex: Int, bitValue: Boolean): Long {
    if (bitIndex !in 0 until Long.SIZE_BITS) throw IndexOutOfBoundsException("Invalid bit index $bitIndex for Long")
    val position = bitIndex
    return ((this and (0x1L shl position).inv()) + ((if (bitValue) 0x1L else 0x0L) shl (position)))
}

/**
 * Gets the [byteIndex]th [Byte]
 *
 * @param [byteIndex] The byte to get
 *
 * @return [Byte]
 */
fun Long.getByte(byteIndex: Int): Byte =
    if (byteIndex in 0 until Long.SIZE_BYTES)
        (this and (0xFFL shl (Byte.SIZE_BITS * byteIndex)) ushr (Byte.SIZE_BITS * byteIndex)).toByte()
    else
        throw IndexOutOfBoundsException("Invalid byte index $byteIndex for Long")



/**
 * Sets the [byteIndex]th byte in the number to [byteValue]
 *
 * @param [byteIndex] The index of the byte to set
 * @param [byteValue] The value to set the byte to
 *
 * @return [Long]
 */
fun Long.setByte(byteIndex: Int, byteValue: Byte): Long {
    if (byteIndex !in 0 until Long.SIZE_BYTES) throw IndexOutOfBoundsException("Invalid byte $byteIndex for Int")
    val position = (Byte.SIZE_BITS * byteIndex)
    return (this and (0xFFL shl (position)).inv()) + (byteValue.toLong() shl (position))
}

operator fun Long.component1(): Byte = getByte(0)

operator fun Long.component2(): Byte = getByte(1)

operator fun Long.component3(): Byte = getByte(2)

operator fun Long.component4(): Byte = getByte(3)

operator fun Long.component5(): Byte = getByte(4)

operator fun Long.component6(): Byte = getByte(5)

operator fun Long.component7(): Byte = getByte(6)

operator fun Long.component8(): Byte = getByte(7)



/**
 * Gets the [shortIndex]th [Short]
 *
 * @param [shortIndex] The byte to get
 *
 * @return [Short]
 */
fun Long.getShort(shortIndex: Int): Short =
    if (shortIndex in 0 until (Long.SIZE_BYTES / Short.SIZE_BYTES))
        (this and (0xFFFFL shl (Short.SIZE_BITS * shortIndex)) ushr (Short.SIZE_BITS * shortIndex)).toShort()
    else
        throw IndexOutOfBoundsException("Invalid short index $shortIndex for Long")

/**
 * Returns the result of setting the [shortIndex]th serial [Short] to [shortValue]
 *
 * @param [shortIndex] The short to set
 * @param [shortValue] The value to set the [shortIndex]th [Short] to
 *
 * @return [Long]
 */
fun Long.setShort(shortIndex: Int, shortValue: Short): Long {
    if (shortIndex !in 0 until (Long.SIZE_BYTES / Short.SIZE_BYTES)) throw IndexOutOfBoundsException("Invalid short index $shortIndex for Long")
    val position = (Short.SIZE_BITS * shortIndex)
    return (this and (0xFFFFL shl position).inv()) + (shortValue.toLong() shl position)
}



/**
 * Gets the [intIndex]th [Int]
 *
 * @param [intIndex] The byte to get
 *
 * @return [Int]
 */
fun Long.getInt(intIndex: Int): Int =
    if (intIndex in 0 until (Long.SIZE_BYTES / Int.SIZE_BYTES))
        (this and (0xFFFFFFFFL shl (Int.SIZE_BITS * intIndex)) ushr (Int.SIZE_BITS * intIndex)).toInt()
    else
        throw IndexOutOfBoundsException("Invalid int index $intIndex for Long")

/**
 * Returns the result of setting the [intIndex]th serial [Int] to [intValue]
 *
 * @param [intIndex] The int to set
 * @param [intValue] The value to set the [intIndex]th [Int] to
 *
 * @return [Long]
 */
fun Long.setInt(intIndex: Int, intValue: Int): Long {
    if (intIndex !in 0 until (Long.SIZE_BYTES / Int.SIZE_BYTES)) throw IndexOutOfBoundsException("Invalid int index $intIndex for Long")
    val position = (Int.SIZE_BITS * intIndex)
    return (this and (0xFFFFFFFFL shl position).inv()) + (intValue.toLong() shl position)
}